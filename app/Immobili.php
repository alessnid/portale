<?php

namespace App;
use App\Favorite;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Immobili extends Model
{

  public function GetPathAttribute(){
    $url = $this->image;
    if(!stristr($url,'http')){
      $url = 'storage/'.$this->image;
    }
    return $url;
  }

  public function getIsfavoriteAttribute(){
    $favorite = Favorite::where('user_id',Auth::user()->id)->where('immobile_id',$this->id)->get();
    if(!$favorite->isEmpty()){
      return true;
    }
    return false;
  }
}
