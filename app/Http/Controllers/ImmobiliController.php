<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Immobili;
use DB;
use Illuminate\Support\Facades\Auth;
use App\Favorite;

class ImmobiliController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $req)
    {
      if($req->has('ordine') && $req->has('ordine2') ){
        $immobili = Immobili::orderBy($req->get('ordine'),$req->get('ordine2'))->get();
      }
        elseif($req->has('ordine')){
          $immobili = Immobili::orderBy($req->get('ordine'),'ASC')->get();
        }
        else {
        $immobili = Immobili::orderBy('created_at','DESC')->get();
      }
        return view('pages.home',['immobili' => $immobili]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.addImmobile');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $immobile = new Immobili();
        $immobile->name = $request->name;
        $immobile->description = $request->description;
        $immobile->image= "";
        $immobile->price = $request->price;
        $immobile->mq = $request->metri;
        $immobile->user_id = Auth::user()->id;
        $res = $immobile->save();
        if($res){
          $resStore = $this->StoreImage($immobile->id,request(),$immobile);
        if($resStore)
          $res = $immobile->save();
        }
        return redirect()->to('/');

    }

    public function details(Immobili $immobile){
      return view('pages.details',['immobile'=>$immobile]);
    }

    // public function favorite(){
    //   return view('pages.home',['immobili' => $immobili]);
    // }


    public function addFavorite($immobile, Request $request){
      $favorite = new Favorite();
      $favorite->immobile_id = $immobile;
      $favorite->user_id = Auth::user()->id;
      $favorite->save();
      return "true";
    }

    public function removeFavorite($immobile){
      $res = Favorite::where('immobile_id',$immobile)->where('user_id',Auth::user()->id)->delete();
      return $res;
    }

    public function StoreImage($id,Request $req, &$immobile){
      if(!$req->hasFile('path'))
        return false;


      $file = $req->file('path');
      if(!$file->isValid())
        return false;
      //$fname = $file->store(env('ALBUM_THUMB_DIR')); NOME CASUALE
      $fname = $id.'.'.$file->extension();
      $fname = $file->storeAs(env('IMG_DIR'),$fname);
      $immobile->image = $fname;
      return true;
  }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
