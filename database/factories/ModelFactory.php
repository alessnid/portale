<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Immobili;
use App\User;
use Faker\Generator as Faker;

$factory->define(Immobili::class, function (Faker $faker) {
  $random = rand(0,300);
    return [
        'name' =>$faker->name,
        'description' => $faker->text(128),
        'user_id' => User::inRandomOrder()->first()->id,
        'image' => "https://i.picsum.photos/id/".$random."/240/120.jpg",
        'mq' => $random,
        'price' => rand(100000,500000),
    ];
});
