<?php

use App\Immobili;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','ImmobiliController@index');

Auth::routes();

Route::get('/home', 'ImmobiliController@index')->name('home');
Route::get('/{immobile}/immobile','ImmobiliController@details');
// Route::get('/favorite','ImmobiliController@favorite');

Route::group(['middleware' =>'auth'], function(){
  Route::get('/create','ImmobiliController@create');
  Route::post('/save','ImmobiliController@store')->name('save');
  Route::post('/{immobile}/addfavorite','ImmobiliController@addFavorite');
  Route::delete('/{immobile}/removefavorite','ImmobiliController@removeFavorite');
});
