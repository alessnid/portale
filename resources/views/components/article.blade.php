<div class="col-sm-4" id="photo-content">
<div class="card" style="width: 18rem;">
  <img class="card-img-top" src="{{$image_url}}" alt="{{$name}}">
  <div class="card-body">
    <h5 class="card-title">{{$name}}</h5>
    <p>data {{$data}}</p>
    <p>PREZZO: {{$price}} €</p>
    <p>MQ: {{$metri_q}} m</p>
    <h5 class="card-title">Description</h5>
    <p>{{$description}}</p>
    {{$slot}}
  </div>
</div>
</div>
