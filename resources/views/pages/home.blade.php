@extends('templates.layout')

@section('title','PORTALE')

@section('content')
<form class="" action="/" method="get">
  <div class="form-group">
        <select required name="ordine" id="ordine">
          <option value="">ORDINA</option>
          <option value="price">prezzo</option>
          <option value="mq">mq</option>
          <option value="created_at">data</option>
        </select>
        <select required name="ordine2" id="ordine">
          <option value="">--</option>
          <option value="ASC">ASC</option>
          <option value="DESC">DESC</option>
        </select>
        <button type="submit">Invia</button>
  </div>
</form>


@stop

@section('contentblog')
  @forelse ($immobili as $immobile )
    @component('components.article')
      @slot('image_url',asset($immobile->path))
      @slot('name', $immobile->name)
      @slot('data', $immobile->created_at)
      @slot('price', $immobile->price)
      @slot('metri_q', $immobile->mq)
      @slot('description', $immobile->description)
        <div class="" style="margin-bottom:8px;">
          <a  href="{{$immobile->id}}/immobile" class="btn btn-primary"> DETTAGLI</a>
        </div>
        @guest
        @else
          @if ($immobile->isfavorite)
            <div class="test2">
              <a id="fav2" href="{{$immobile->id}}/removefavorite" class="btn btn-danger"> RIMUOVI DAI PREFERITI </a>
            </div>
          @else
              <div class="test">
                <a id="fav" href="{{$immobile->id}}/addfavorite" class="btn btn-primary"> Favorite </a>
              </div>
          @endif
        @endguest

    @endcomponent
  @empty
    <h1>NO IMAGES</h1>
  @endforelse
@stop

@section('script')
  @parent
  <script type="text/javascript">
    $('document').ready(function(){
      $('.test').on('click','#fav',function(e){
        e.preventDefault();
        div = $(this);
        $.ajax({
          url: $(this).attr('href'),
          type: 'POST',
          data: {
            '_token' : '{{csrf_token()}}'
          },
          success: function(out){
            alert('aggiunto ai preferiti');
            div.remove();
          }
        });
      });

      $('.test2').on('click','#fav2',function(e){
        e.preventDefault();
        div = $(this);
        $.ajax({
          url: $(this).attr('href'),
          type: 'DELETE',
          data: {
            '_token' : '{{csrf_token()}}'
          },
          success: function(out){
            alert('eliminato dai preferiti');
            div.remove();
          }
        });
      });
    });
  </script>
@stop
