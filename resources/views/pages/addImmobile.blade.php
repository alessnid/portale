@extends('templates.layout')

@section('name','CREATE')

@section('content')
<h1>NUOVO IMMOBILE</h1>
  <form class="" action="{{route('save')}}" method="POST" enctype="multipart/form-data">
    {{csrf_field()}}
    <div class="form-group">
      <label for="">Nome</label>
      <input name="name" type="text" class="form-control" required placeholder="Nome">
    </div>
    <div class="form-group">
      <label for="">Descrizione</label>
      <input name="description" type="text" class="form-control" required placeholder="Descrizione">
    </div>
    <div class="form-group">
    <label for="">Image</label>
    <input name="path" type="file" class="form-control" placeholder="Name" value="">
    </div>
    <div class="form-group">
      <label for="">Prezzo</label>
      <input name="price" type="number" min="50000" step="10000" class="form-control" required placeholder="Prezzo">
    </div>
    <div class="form-group">
      <label for="">Metri Quadri</label>
      <input name="metri" type="number" min="60" step="10" class="form-control" required placeholder="Metri Quadri">
    </div>
    <button class="btn btn-primary" type="submit">INVIA</button>
  </form>

@endsection
